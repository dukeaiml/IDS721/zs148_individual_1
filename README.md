# Individual Project 1
## Author
Ziyu Shi

## Project Steps
### Prepare for the required environment
1. Install `Zola` in Ubuntu:
    ```
    sudo snap install --edge zola
    ```
    Ensuring that `snap` has been installed on the system, or install it by:
    ```
    sudo apt install snapd
    ```
2. Verify if `Zola` has been installed properly:
    ```
    zola --version
    ```

### Create and design the zola site
1. Create the zola site by `zola init <YOUR_SITE>`
2. Download the theme to the theme directory
    ```
    cd <YOUR_SITE>/themes
    git clone <GIT_THEME_URL>
    ```
3. Enable the theme in the config.toml
4. Design the content of the site
5. (Optional) Build the site by `zola build`
6. Test the site at http://127.0.0.1:1111 by `zola serve`

### Build and Deploy the site on GitLab
1. Ensuring that the runner can access the theme
    ```
    git submodule add <HTTP_THEME_URL> themes/<THEME_NAME>
    ```
2. Setting up the **GitLab CI/CD pipeline**. Create a file named `.gitlab-ci.yml` in the root directory of the repository and add deployment commands. The **GitLab CI/CD pipeline** has been set in the `.gitlab-ci.yml`:
    ```yaml
    stages:
        - deploy

    default:
        image: debian:stable-slim
        tags:
            - docker

    variables:
        # The runner will be able to pull your Zola theme when the strategy is
        # set to "recursive".
        GIT_SUBMODULE_STRATEGY: "recursive"

        # If you don't set a version here, your site will be built with the latest
        # version of Zola available in GitHub releases.
        # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
        ZOLA_VERSION:
            description: "The version of Zola used to build the site."
            value: ""

    pages:
        stage: deploy
        script:
            - |
            apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
            if [ $ZOLA_VERSION ]; then
                zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
                if ! wget --quiet --spider $zola_url; then
                echo "A Zola release with the specified version could not be found.";
                exit 1;
                fi
            else
                github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
                zola_url=$(
                wget --output-document - $github_api_url |
                grep "browser_download_url.*linux-gnu.tar.gz" |
                cut --delimiter : --fields 2,3 |
                tr --delete "\" "
                )
            fi
            wget $zola_url
            tar -xzf *.tar.gz
            ./zola build
    ```

3. Please keep in mind that this script will assume that the GitLab Runner uses the Docker executor. After pushing the project to the default branch of the repository, the GitLab CI/CD pipelines will ensure your site is published and updated automatically. The URL of the website can be found at the **Deploy > Pages**. Feel free to adjust the file to your workflow and specific requirements.

### Hosted on Netlify
1. Create a file named `netlify.toml` in the root directory of the project.
2. Add deploy commands in `netlify.toml` and push the project onto the GitLab repo. The content of my deployment file:
    ```toml
    [build]
    # This assumes that the Zola site is in a docs folder. If it isn't, you don't need
    # to have a `base` variable but you do need the `publish` and `command` variables.
    publish = "/public"
    command = "zola build"

    [build.environment]
    # Set the version name that you want to use and Netlify will automatically use it.
    ZOLA_VERSION = "0.18.0"

    [context.deploy-preview]
    command = "zola build --base-url $DEPLOY_PRIME_URL"
    ```
3. Sign in [Netlify](https://app.netlify.com/) and add new sites in **Sites**. Import the repository in the GitLab, the configuration will be filled automatically from the `netlify.toml`.

## Screenshots of my website
### Introduction to my website
**Home page**
![home page](static/photos/home.png)

**About page (portfolio page)**
![about page](static/photos/about.png)
In this page, I introduce myself in detail.

**Education page**
![education page](static/photos/education.png)
In this page, I introduce my undergraudate university and graudate university.

**Skills page** 
![Skills page](static/photos/skills.png)
In this page, I introduce the programming skills I learned.

**Projects**
![Projects page](static/photos/projects.png)
In this page, I introduce two projects accomplished during Duke.

### Deploy on GitLab
**Access website URL**
![image](static/photos/deployGitlab.png)

**Website deployed on GitLab**
![image](static/photos/gitlabURL.png)

### Hosted on Netlify
**Access webstie URL**
![image](static/photos/deployNetlify.png)

**Website deployed on Netlify**
![image](static/photos/netlifyURL.png)
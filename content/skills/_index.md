+++
title = "Skills"
template = "skills.html"
page_template = "skills.html"

[extra]
author = "Ziyu Shi"
image = "https://seeklogo.com/images/P/Pikachu-logo-D0AAA93F17-seeklogo.com.png"

lan = [
{ lang = "C++ Programming", expr = "4", image = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/1200px-ISO_C%2B%2B_Logo.svg.png", link = "https://cplusplus.com/"}, 
{ lang = "C Programming", expr = "3", image = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/C_Programming_Language.svg/695px-C_Programming_Language.svg.png", link = "https://cplusplus.com/"}, 
{ lang = "Python Programming", expr = "3", image = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/800px-Python-logo-notext.svg.png", link = "https://www.python.org/"}, 
]

tools = [
{ tool = "Linux", expr = "4", image = "https://cdn.pixabay.com/photo/2017/01/31/15/33/linux-2025130_640.png", link = "https://www.linux.org/"},
{ tool = "Django", expr = "2", image = "https://images.ctfassets.net/23aumh6u8s0i/6ubUHRD1qfolOVHxiBfjZ7/4e704f48dc5b0104d0c380fec1fe9b9e/django", link = "https://www.djangoproject.com/"},
{ tool = "Valgrind", expr = "3", image = "https://www.scientiamobile.com/page/wp-content/uploads/2014/05/valgrind.jpg", link = "https://valgrind.org/"},
]
+++
